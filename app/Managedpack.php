<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Managedpack extends Model
{
    public function hostpack() {
        return $this->hasOne('App\Hostpack', 'managedpack_id', 'id');
    }
}
