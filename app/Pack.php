<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    public function hostpacks() {
        return $this->belongsToMany('App\Hostpack');
    }
}
