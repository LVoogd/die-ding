<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function hostpacks() {
        return $this->hasMany('App\Hostpack', 'customer_id');
    }
    
}
