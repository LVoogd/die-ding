<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\StaticHostpack;
use App\StaticManagedpack;
use App\Customer;
use App\Pack;
use App\Managedpack;


class HostpackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hostpacks = Hostpack::all();
            
        return view('hostpacks.index')->with('hostpacks', $hostpacks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hostpack($id)
    {
        $customer = Customer::where('id', '=', $id)->first();
        
        $hostpacks = Customer::find($id)->hostpacks;
        
        $count = 0;
        $revenue = 0;
        
        foreach ( $hostpacks as $hostpack ) {
         
            $pack_id = $hostpack->static_hostpack_id; 
            $managedpack_id = $hostpack->managedpack_id;
                 
            $staticHostpack = StaticHostpack::where('id', $pack_id)->first();
            $managedpack = Managedpack::where('id', $managedpack_id)->first();
            
            if ($managedpack == '') {
                $hostpack['managed_price']   = 0;
                $hostpack['is_managed']      = 'glyphicon glyphicon-remove';
            } else {
                $staticManagedpack = StaticManagedpack::where('id', $managedpack->static_managedpack_id )->first();
                $hostpack['managed_price']   = $staticManagedpack->price;
                $hostpack['is_managed']      = 'glyphicon glyphicon-ok';
            }
                        
            $hostpack['hostpack_price']  = $staticHostpack->price;
            $hostpack['total_price']     = $hostpack['hostpack_price'] + $hostpack['managed_price'];
            
            $hostpack_all[] = $hostpack;
            
            $revenue = $revenue + $hostpack['total_price'];
                                
        }
        
        if (empty($hostpack_all)) { $hostpack_all = ''; }
        $customer['revenue'] = $revenue; 
                
        return view('hostpacks.index')->with(array(
            'customer'  => $customer,
            'hostpacks' => $hostpack_all
        ));
    }
    
    public function details($customerId, $hostpackId) {
        
        return view('hostpacks.details')->with(array(
            'customer' => $customer
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
