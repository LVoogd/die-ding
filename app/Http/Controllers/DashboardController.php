<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hostpack;

class DashboardController extends Controller
{    
    public function index() {
        
        $expires = Hostpack::Where('id', '>', 0)->orderBy('expire_date', 'asc')->take(5)->get();
            
        return view('dashboard/index')->with(array(
            'expires' => $expires
        ));
    }
}
