<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    // Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
    
 
});

Route::group(['middleware' => [ 'web', 'auth']], function () {
    
    // Dashboard routes
    Route::get('/', 'DashboardController@index');  
    Route::get('dashboard', 'DashboardController@index');

    // HostpackController routes
    Route::get('customer/{id}/hostpack', 'HostpackController@hostpack');
    Route::get('customer/{customerId}/hostpack/{hostpackId}/details', 'HostpackController@details');
    
    // CustomerController
    Route::get('customer/create', 'CustomerController@create'); 
    Route::get('customer/{id}/edit', 'CustomerController@edit'); 
    Route::resource('customer', 'CustomerController');

});

