<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hostpack extends Model
{
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
    
    public function pack() {
       return $this->hasMany('App\StaticHostpack');
        
    }
}


