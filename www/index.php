<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="description" content="Alles voor uw website onder één dak.">
        <meta name="keywords" content="Webdesign, Hosting, Ouddorp, Flakkee, betaalbaar, creatief, flexibel">
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="1 month">
        <title>Cloudstronauts - Webdesign en Hosting</title>
        <!-- Webdesign, Hosting, Flakkee -->
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/style.css">
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    </head>

<body>
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#"><img class="nav-logo" src="images/navlogo.png" alt="nav logo"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" id="contactscroll">Contact</a></li>
                    <li><a href="http://webmail.cloudstronauts.nl">Webmail</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="header">
        <img class="headerlogo" src="images/headerlogo.png" alt="header logo">
            <p class="form-header-content" style="padding-top: 30px">Weten hoeveel een website bij ons kost?</p>
            <div style="text-align: center; padding-bottom: 20px;"><a href="/form.php" class="button blue">Bereken!</a></div>
    </div>
    <div class="row welcome-row">
        <div class="col-md-10 col-md-offset-1">
        <h2 class="welcome-title">Alles voor uw website onder één dak</h2>
            <p class="welcome-content">
            Een centraal punt voor alle zaken die bij een website komen kijken. Het maken van een website maar ook het online zetten en                 onderhouden hiervan kunt u allemaal bij ons afnemen, voor u op maat gemaakt. En dat allemaal onder één dak.
            </p>
        </div>
    </div>
    <div class="solution-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img class="solutionbox" src="images/flexibellogo.png" alt="header logo">
                    <p class="solutiontext">Flexibel</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img class="solutionbox" src="images/goedkooplogo.png" alt="header logo">
                    <p class="solutiontext">Betaalbaar</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img class="solutionbox" src="images/creatieflogo.png" alt="header logo">
                    <p class="solutiontext">Creatief</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row our-service">
            <div class="col-md-5 col-md-offset-1">
            <h2 class="welcome-title">Voor u en uw hobby</h2>
                <p class="welcome-content special-solution">
                    Bloemschikken, fotografie, schilderen... noem het maar op! Met een website kunt u laten zien wat u bezig houdt en dit                     delen met de rest van de wereld.
                </p>
            </div>
            <div class="col-md-5">
            <h2 class="welcome-title">Voor starters en veteranen</h2>
                <p class="welcome-content">
                    Een website is een online visitekaartje voor uw onderneming en niet meer weg te denken in het bedrijfsleven. Met een                     website kunt u kort en krachtig laten zien wat uw onderneming allemaal in zijn mars heeft.   
                </p>
            </div>
            </div>
        </div>
    </div>
    
    <hr class="first-line">
    <hr class="seccond-line">
    
    <div class="container">
        <div class="row contact-row">
            
            <div class="col-md-5 col-md-offset-1">
                <p class="contact-text">U kunt ons altijd e-mailen voor een afspraak!</p>
                <form action="/contact-to-email.php" method="post">
                    <input type="text" class="form-control input required" name="voornaam" placeholder=" *Voornaam" style="max-width: 250px;" />
                    <input type="text" class="form-control input required" name="achternaam" placeholder=" *Achternaam" style="max-width: 250px;"/>
                    <input type="text" class="form-control input" name="bedrijfsnaam" placeholder=" Bedrijfsnaam" style="max-width: 250px;"/>
                    <input type="text" class="form-control input required" name="email" placeholder=" *E-mail adres" style="max-width: 250px;"/>
                    <textarea  class="form-control input" name="textarea" rows="4" cols="150"></textarea>
                    <input type="submit" class="button blue" id="btnSubmit" value="Verzenden" />
                    <p class="antispam">Laat dit leeg: <input type="text" name="url" /></p>
                    
                </form>
            </div>
            <div class="col-md-5">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2466.822503067381!2d3.941101500000001!3d51.8094038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c4f874e497c58f%3A0xdd54e010bb1bb24d!2sDiependorst+121%2C+3253+VC+Ouddorp!5e0!3m2!1snl!2snl!4v1418819231239" frameborder="0" style="border:0"></iframe>
                <p><strong>E-mail:</strong>info@cloudstronauts.nl - <strong>KvK:</strong>62011707</p>

                
            </div>
        </div>
    </div>
    
                
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
   
</body>
</html>

<script>
     $('#btnSubmit').click(function(e) {

        var isValid = true;
        $('input[type="text"].required').each(function() {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid #FF2E12",
                });
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        if (isValid == false) 
            e.preventDefault();
 
    });
        $('a[href^="#"]').click(function() {
    $('html,body').animate({
        scrollTop: $(".first-line").offset().top},
        'slow');
});

</script>
