
@extends('layout.app')

@section('content')
<head>
    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
</head>
    <div class="login-bg">
        <div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>Login to Your Account</h1><br>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="/auth/login">
                        {!! csrf_field() !!}
                        
					<input type="email" name="email" placeholder="Email" value="">
					<input type="password" name="password" placeholder="Password" id="password">
                    <input type="checkbox" name="remember"> Remember Me
                        <input type="submit" name="login" class="login loginmodal-submit" value="Login">
				  </form>
				  <div class="login-help">
					<a href="{{ URL::to('auth/register') }}">Register</a> - <a href="#">Forgot Password</a>
				  </div>
				</div>
			</div>
    </div>
@stop