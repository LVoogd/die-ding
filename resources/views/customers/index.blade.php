@extends('layout.app')

@section('content')

        <div class="heading">
            <h2>Customers</h2>
         </div>
            <div class="navbar-left">
            <ul>
                <a href="/dashboard"><li class="navbar-left-li">Dashboard</li></a>
                <a href="/customer"><li class="navbar-left-li-active">Customers</li></a>
            </ul>         
            </div>
        <div class="wrapper">
            <div class="container-fluid">
                <a href="/customer/create">
                    <button class="btn btn-default">Add Customer</button>
                </a>
            </div>            
        <div class="table-wrapper">
            <table class="table table-striped task-table">
                    
                    <thead>
                        <th></th>
                        <th>ID</th>
                        <th>Name</th>
                    </thead>
                <tbody>
                    @foreach($customers as $customer)
                    <tr>
                        <td class="table-small">
                        <a href="/customer/{{ $customer->id }}/hostpack ">                       
                            <button class="btn btn-default">View</button>
                        </a>
                        </td>
                        <td class="table-small">
                            <p>{{ $customer->customer_id}}</p>
                        </td>
                        <td class="table-text">
                            <p>{{ $customer->first_chars }} {{ $customer->last_name }}<p>
                        </td>
                    </tr>
                     @endforeach


                </tbody>
        </table>
        </div>
    </div>
                  
                    
@stop
