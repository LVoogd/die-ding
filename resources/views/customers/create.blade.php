@extends('layout.app')

@section('content')

<script src="{{ URL::asset('js/validator.js') }}"></script>
        <div class="heading">
            <h2>Create Customer</h2>
         </div>
            <div class="navbar-left">
            <ul>
                <a href="/dashboard"><li class="navbar-left-li">Dashboard</li></a>
                <a href="/customer"><li class="navbar-left-li-active">Customers</li></a>
            </ul>
            </div>
            <div class="wrapper">
                <div class="container">
                        {!! Form::open(array(
                            'route' => 'customer.store',
                            'data-toggle' => 'validator'
                        ))
                    !!}
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="text-underline">Personal details</h2>
                                <div class="form-group">
                                    <label for="first_name">First name <span class="required-asterisk">*</span></label>
                                    {!! Form::text('first_name', '', array(
                                        'class'     => 'form-control',
                                        'required'  => 'required',
                                        'id'        => 'first_name',
                                        'data-error'=> 'This field is required'
                                    )) 
                                    !!}
                                 <div class="help-block with-errors"></div>    
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last name <span class="required-asterisk">*</span></label>           
                                    {!! Form::text('last_name', '', array(
                                        'class'    => 'form-control',
                                        'required' => 'required',
                                        'data-error'=> 'This field is required'
                                    )) 
                                    !!}
                                 <div class="help-block with-errors"></div>                                        
                                </div>
                                <h4>Gender <span class="required-asterisk">*</span></h4>
                                
                            <div class="radio">
                                {!! Form::radio('gender', '1', true, array(
                                        'id'       => 'radio1'
                                    )) 
                                !!}
                                <label for="radio1">
                                    Male
                                </label>
                            </div>
                                
                            <div class="radio">
                                {!! Form::radio('gender', '0', false, array(
                                        'id'       => 'radio2'
                                    )) 
                                !!}
                                <label for="radio2">
                                    Female
                                </label>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="panel panel-body panel-default">
                                <h2 class="text-underline">Business details <span style="font-size: 12px;">(optional)</span></h2>
                                <div class="form-group">
                                    {!! Form::label('business_name', 'Business name') !!}
                                    {!! Form::text('business_name', '', array(
                                        'class'    => 'form-control',
                                    )) 
                                    !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('kvk', 'KvK number') !!}
                                    {!! Form::text('kvk', '', array(
                                        'class'    => 'form-control',
                                        'maxlength' => '8',
                                        'pattern'  => '[\d]*'
                                    )) 
                                    !!}
                                </div>
                            </div>
                            </div>
                        </div>
                <h2 class="text-underline">Contact details</h2>
                        <div class="row">
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="email">E-mail <span class="required-asterisk">*</span></label>           
                                     {!! Form::email('email', '', array(
                                        'class'     => 'form-control',
                                        'required'  => 'required',
                                        'id'        => 'email',
                                        'data-error'=> 'Enter a valid e-mail address'
                                     ))
                                     !!}
                                    <div class="help-block with-errors"></div>
                                 </div>
                                
                                <div class="form-group">
                                    {!! Form::label('phone', 'Phone number') !!}
                                    {!! Form::text('phone', '', array(
                                        'class'     => 'form-control',
                                    ))
                                    !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit('Add Customer', array( 
                            'class' => 'btn btn-default'
                        )) !!} 
                        
                        {!! Form::close() !!}
                    </div>

                </div>                

@stop
