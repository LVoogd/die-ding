
@extends('layout.app')

@section('content')
    <div class="heading">
        <h2>Dashboard</h2>
     </div>
 <div class="navbar-left">
            <ul>
                <a href="/dashboard"><li class="navbar-left-li-active">Dashboard</li></a>
                <a href="/customer"><li class="navbar-left-li">Customers</li></a>
            </ul>         
    </div>
        <div class="wrapper">
              <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><i class="fa fa-exclamation-circle"></i>
 About to expire</h4>
                        </div>
                <div class="panel-body">
                    <table class="table table-striped task-table">
                        <thead>
                            <th>Domain name</th>
                            <th>Expires</th>
                            <th></th>
                        </thead>
                    <tbody>
                    @foreach ($expires as $expire)
                                
                    <tr>
                        <td>{{ $expire->domain_name }}</td>
                        <td>{{ $expire->expire_date }}</td>
                        <td class="table-small">
                        <a href="/customer/{{ $expire->customer_id }}/hostpack/{{ $expire->id }}/details ">                      
                            <button class="btn btn-default">Details</button>
                        </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>  
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
              </div>  
        </div>

@stop