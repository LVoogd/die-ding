@extends('layout.app')

@section('content')

        <div class="heading">
            <h2> {{ $customer->first_name }} {{ $customer->last_name }} <span style="font-size: 15px;">{{ $customer->business_name }}</span></h2>
         </div>
        <div class="navbar-left">
            <ul>
                <a href="/dashboard"><li class="navbar-left-li">Dashboard</li></a>
                <a href="/customer"><li class="navbar-left-li-active">Customers</li></a>
            </ul>          
        </div>
        <div class="wrapper">

            @if (session('message'))
                <div class="message-panel {{ $messageStatus }}">
                    <i class="fa fa-flag"></i> {{ session(message) }}
                </div>
            @endif
            <div class="container-fluid">
                <a href="/customer/{{ $customer->id }}/edit">
                    <button class="btn btn-default">Edit Customer</button>
                </a>
            </div>
        <div class="container">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-sm-8">
                <table>
                <tr>
                    <td>
                        <p>Name: {{ $customer->first_name }} {{ $customer->last_name}} </p>
                        <p>Business: {{ $customer->business_name }} </p>
                        <p>E-mail: {{ $customer->email }} </p>
                    </td>
                    <td>  </td>
                    <td>
                        <p> Gender: </p>
                        <p> Gender: </p>
                        <p> Gender: </p>
                    </td>
                </tr>
                </table>
                </div>
                <div class="col-sm-4 text-center">
                    <p>Total revenue<p>
                    <h2>€ {{ $customer->revenue }}</h2>
                </div>
            </div>    
        </div>
        </div>
    <div class="container-fluid">
        @if (empty($hostpacks))
        <div class="container">
            <div class="panel panel-body panel-info">
                <h3 class="text-center">This customer has no Hostpacks yet.</h3>
            </div>
        </div>
        @else
        
      <div class="table-wrapper">
    <table class="table table-striped task-table">
                        <thead>
                            <th></th>
                            <th>Hostpack ID</th>
                            <th>Domain name</th>
                            <th>Expires</th>
                            <th class="text-center">Managed</th>
                            <th>Cost</th>
                        </thead>
                    <tbody>
                        @foreach($hostpacks as $hostpack)
                        
                        <tr>
                            <td class="table-small" style="width: 100px;">
                                <button class="btn btn-default">Details</button>
                            </td>
                            <td class="table-small">
                                <p>{{ $hostpack->id }}</p>
                            </td>
                            <td>
                                <p><a href="http://{{ $hostpack->domain_name }}" target="_blank">{{ $hostpack->domain_name }}</a></p>
                            </td>
                             <td>
                                <p>{{ $hostpack->expire_date }}</p>   
                             </td>

                            <td class="table-small text-center">
                                <span class="{{ $hostpack->is_managed }}"></span>
                            </td>
                             <td>
                                 <p>{{ $hostpack->total_price }}</p>
                             </td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
          @endif
        </div>
    </div>
</div>
                  
                    
@stop
